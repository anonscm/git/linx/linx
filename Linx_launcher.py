# -*- coding: utf-8 -*-

import sys
import os
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QCoreApplication
import PyQt5.uic
import Linx_core


def compile_if_necessary(input_ui_file, output_py_file):
    """
    recompile the .ui Qt file if the compiled .py GUI file does not exist
    or if it is more recent than the compiled .py GUI file
    """
    # prepare the file names
    input_path = input_ui_file
    output_path = output_py_file
    if not(os.path.isfile(output_path)) or os.path.getmtime(input_path) > os.path.getmtime(output_path):
        # need to specify utf8 otherwise it assumes ascii by default and
        # it cannot write greek letter symbols that are in the .ui file
        with open(output_path, encoding='utf-8', mode="w") as outf:
            print("An update to the GUI was detected: recompiling "+input_ui_file)
            PyQt5.uic.compileUi(input_path, outf)


# get reference to the running QApplication (if any, otherwise return None)
app = QCoreApplication.instance()


def main():
    new_app = False
    global app
    if app is None:
        # no running QApplication, starts one
        # and pass it to the command line arguments
        new_app = True
        app = QApplication(sys.argv)
    app.references = set()
    # create the Graphical User Interface main window
    window = Linx_core.MainApp()
    app.references.add(window)
    window.show()
    # if a QApplication was just created
    # launch the Qt event manager through "app.exec()" (no more sys.exit)
    if new_app:
        app.exec()


if __name__ == '__main__':
    compile_if_necessary("Linx_gui.ui", "Linx_gui_Ui.py")
    main()
