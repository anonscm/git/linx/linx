﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Linx.py
Lock-in numerique

Created by X. FABREGES on 2012-02-13.
Modified: 2014-01-23

"""

# system imports
import time
# from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMainWindow, QFileDialog
from PyQt5.QtCore import QCoreApplication
# GUI
import Linx_gui_Ui
# number crunching imports
import numpy as np
import scipy as sp
import scipy.optimize
import scipy.integrate
import scipy.signal as signal
# error free scipy butter
import modified_filter
import Linx_gui_getset

VERSION = 'Version: 2019-09-09'


class Switch(object):
    """Switch/case like class"""
    def __init__(self, value):
        self.value = value
        self.fall = False

    def __iter__(self):
        yield self.match
        raise StopIteration

    def match(self, *args):
        if self.fall or not args:
            return True
        elif self.value in args:
            self.fall = True
            return True
        else:
            return False


class MainApp(QMainWindow):
    def __init__(self):
        # This class derivates from a Qt MainWindow so we have to call
        # the class builder ".__init__()"
        QMainWindow.__init__(self)
        # "self" is now a Qt Mainwindow, then we load the user interface
        # generated with QtDesigner and call it self.ui
        self.ui = Linx_gui_Ui.Ui_MainWindow()
        # Now we have to feed the GUI building method of this object (self.ui)
        # with a Qt Mainwindow, but the widgets will actually be built as children
        # of this object (self.ui)
        self.ui.setupUi(self)
        self.setWindowTitle(VERSION)

        # give the reference "self" (the main thread) to the fixed plot windows
        self.ui.plot_signal_left.parent = self
        self.ui.plot_signal_right.parent = self
        self.ui.plot_field_left.parent = self
        self.ui.plot_field_right.parent = self
        # Initialization of pyqtgraph area
        psl = self.ui.plot_signal_left.getPlotItem()
        psl.setLabels(title="Raw Signal", left="Raw Voltage (V)", bottom="Time (s)")
        psr = self.ui.plot_signal_right.getPlotItem()
        psr.setLabels(title="Signal", left="Resistance (Ohms)", bottom="Field (T)")
        pfl = self.ui.plot_field_left.getPlotItem()
        pfl.setLabels(title="Ref. Signal", left="Ref. Voltage (V)", bottom="time (s)")
        pfr = self.ui.plot_field_right.getPlotItem()
        pfr.setLabels(title="Magnetic field", left="B (T), PU (V)", bottom="time (s)")
        self.ui.plot_signal_left.addLegend()
        self.ui.plot_signal_right.addLegend()
        self.ui.plot_signal_left.setDownsampling(auto=True, mode='peak')
        self.ui.plot_signal_left.setClipToView(True)
        self.ui.plot_signal_left.setDownsampling(auto=True, mode='peak')
        # TODO : display bugs if this downsampling is ON, signal/slot interference ??
        # self.ui.plot_signal_right.setDownsampling(auto=True, mode='peak')
        self.ui.plot_signal_left.setClipToView(True)
        # self.ui.plot_signal_right.setClipToView(True)
        self.ui.plot_field_left.addLegend()
        self.ui.plot_field_right.addLegend()

        # Instantiate a getter/setter class to fetch the parameters in the various GUI elements
        self.guiio = Linx_gui_getset.Getset(self.ui)
        # Global variables
        self.data = 0
        self.data_loaded = 0
        self.input_dir = ''
        self.output_dir = ''

        # Raw File Options GroupBox
        self.colnb = int(self.ui.ed_ncol.text())
        self.phasemin = np.zeros(self.colnb - 2)
        self.coldbdt = int(self.ui.ed_dBdtcol.text())
        self.coldatai = np.fromstring(self.ui.ed_datcol.text(), dtype=int, sep=',')
        self.colrefi = np.fromstring(self.ui.ed_refcol.text(), dtype=int, sep=',')
        self.colref = np.fromstring(self.ui.ed_refcol.text(), dtype=int, sep=',')

        # Experimental parameters
        self.sample_rate = 500000.0
        self.current = 1.0
        self.pu_area = 4.666e-4
        self.gain = 200

        self.cutoff = 1000
        self.dephase = 0

        self.dbdted = 0

        self.zp = 2
        self.UI_cutoffHz()

    def f_loadconfig(self):
        """ Open configuration file, modify every field accordingly """
        inputfile, _ = QFileDialog.getOpenFileName(self, "Open Configuration File", self.output_dir)
        if inputfile != '':
            finput = open(str(inputfile), 'r')

            for tempstring in finput.read().split('\n'):
                tempstring = tempstring.split()
                for i in range(0, len(tempstring)):
                    tempstring[i] = tempstring[i].strip(' = ')

                if len(tempstring) > 0:
                    for case in Switch(tempstring[0]):
                        if case('#'):
                            break
                        if case('inputfile'):
                            self.input_dir = tempstring[2]
                            break
                        if case('outputfile'):
                            self.output_dir = tempstring[2]
                            break
                        if case('cutoff'):
                            self.ui.ed_cutoff.setText(tempstring[2])
                            break
                        if case('phase'):
                            self.ui.ed_manphase.setText(tempstring[2])
                            break
                        if case('pickup'):
                            self.ui.ed_PUarea.setText(tempstring[2])
                            break
                        if case('sample_rate'):
                            self.ui.ed_srate.setText(tempstring[2])
                            break
                        if case('nb_col'):
                            self.ui.ed_ncol.setText(tempstring[2])
                            break
                        if case('col_dbdt'):
                            self.ui.ed_dBdtcol.setText(tempstring[2])
                            break
                        if case('col_ref'):
                            self.ui.ed_refcol.setText(tempstring[2])
                            break
                        if case('current'):
                            self.ui.ed_current.setText(tempstring[2])
                            break
                        if case('gain'):
                            self.ui.ed_gain.setText(tempstring[2])
                            break
                        if case('thinning'):
                            self.ui.ed_thin_out.setText(tempstring[2])
                            break
                        if case('col_data'):
                            self.ui.ed_datcol.setText(tempstring[2])
                            break
                        if case('plot_phase'):
                            if float(tempstring[2]) == 1:
                                self.ui.cb_inphase.setChecked(True)
                                self.ui.cb_outphase.setChecked(False)
                            elif float(tempstring[2]) == 2:
                                self.ui.cb_inphase.setChecked(True)
                                self.ui.cb_outphase.setChecked(False)
                            elif float(tempstring[2]) == 3:
                                self.ui.cb_inphase.setChecked(True)
                                self.ui.cb_outphase.setChecked(True)
                            else:
                                self.ui.cb_inphase.setChecked(False)
                                self.ui.cb_outphase.setChecked(False)
                            break
                        if case('binary'):
                            if float(tempstring[2]) == 1:
                                self.ui.cb_binfile.setChecked(True)
                            if float(tempstring[2]) == 0:
                                self.ui.cb_binfile.setChecked(False)
                            break
                        if case('sub_dbdt'):
                            if float(tempstring[2]) == 1:
                                self.ui.cb_dbdtsub.setChecked(True)
                            break
                        if case('RMS'):
                            if float(tempstring[2]) == 1:
                                self.ui.cb_rms.setChecked(True)
                            break
            del inputfile
            self.UI_cutoffHz()

    def f_saveconfig(self, *args):
        """ Save configuration file based on current GUI status """
        if len(args) == 0:
            inputfile, _ = QFileDialog.getSaveFileName(self, "Save Configuration File", self.output_dir)
        else:
            inputfile = args[0]

        if inputfile != '':
            with open(str(inputfile), 'w') as finput:
                # Ajout des commentaires d'aide pour l'utilisateur
                finput.write('# Linx configuration file\n')
                if self.data_loaded == 1:
                    finput.write('# Files\n')
                    finput.write('inputfile =  ' + str(self.input_dir) + '\n')
                    finput.write('outputfile =  ' + str(self.output_dir) + '\n')
                finput.write('# Cutoff frequency (Hz)\n')
                finput.write('cutoff =  '+str(self.ui.ed_cutoff.text())+'\n')
                finput.write('# Manual phase (deg)\n')
                finput.write('phase =  '+str(self.ui.ed_manphase.text())+'\n')
                finput.write('# Pickup area (m2)\n')
                finput.write('pickup =  '+str(self.ui.ed_PUarea.text())+'\n')
                finput.write('# Sample rate (Hz)\n')
                finput.write('sample_rate =  '+str(self.ui.ed_srate.text())+'\n')
                finput.write('# Number of columns in raw file\n')
                finput.write('nb_col =  '+str(self.ui.ed_ncol.text())+'\n')
                finput.write('# dBdt col. number\n')
                finput.write('col_dbdt =  '+str(self.ui.ed_dBdtcol.text())+'\n')
                finput.write('# Signal reference col. number\n')
                finput.write('col_ref =  '+str(self.ui.ed_refcol.text())+'\n')
                finput.write('# Data from sample col. number\n')
                finput.write('col_data =  '+str(self.ui.ed_datcol.text())+'\n')
                finput.write('# Injected current (mA)\n')
                finput.write('current =  '+str(self.ui.ed_current.text())+'\n')
                finput.write('# Gain/Amp.\n')
                finput.write('gain =  '+str(self.ui.ed_gain.text())+'\n')
                finput.write('# Thinning of save files\n')
                finput.write('thinning =  '+str(self.ui.ed_thin_out.text())+'\n')
                finput.write('# Automatic spikes suppression (experimental)\n')
                finput.write('# Signal displayed/saved (in and/or out of phase)\n')
                finput.write('# 3 = in+out, 2 = out, 1 = in\n')
                if self.ui.cb_inphase.isChecked() and self.ui.cb_outphase.isChecked():
                    finput.write('plot_phase =  3\n')
                elif self.ui.cb_inphase.isChecked():
                    finput.write('plot_phase =  1\n')
                elif self.ui.cb_outphase.isChecked():
                    finput.write('plot_phase =  2\n')
                else:
                    finput.write('plot_phase =  1\n')
                finput.write('# up/down filed displayed/saved\n')
                finput.write('# 3 = up+down, 2 = dwn, 1 = up\n')
                if self.ui.cb_up.isChecked() and self.ui.cb_down.isChecked():
                    finput.write('plot_updwn =  3\n')
                elif self.ui.cb_down.isChecked():
                    finput.write('plot_updwn =  2\n')
                elif self.ui.cb_up.isChecked():
                    finput.write('plot_updwn =  1\n')
                else:
                    finput.write('plot_updwn =  2\n')
                finput.write('# RMS current (1=yes, 0=no)\n')
                if self.ui.cb_rms.isChecked():
                    finput.write('RMS =  1\n')
                else:
                    finput.write('RMS =  0\n')
                finput.write('# Bianry file (1=yes, 0=no)\n')
                if self.ui.cb_binfile.isChecked():
                    finput.write('binary =  1\n')
                else:
                    finput.write('binary =  0\n')
                finput.write('# Remove dBdt from raw data (1=yes, 0=no)\n')
                if self.ui.cb_dbdtsub.isChecked():
                    finput.write('sub_dbdt =  1\n')
                else:
                    finput.write('sub_dbdt =  0\n')

    def f_load_data(self, datafile=''):
        """Load data from user file # the 'clicked' signal of the button always sends a bool, add *args to catch it"""
        inputfile = datafile
        # if inputfile is empty give the user a file dialog
        if not inputfile:
            inputfile, _ = QFileDialog.getOpenFileName(self, "Open Raw Data File", self.input_dir)
        print('inputfile', inputfile)
        # if inputfile is still empty tell the user to select something, otherwise proceed with opening the file
        if not inputfile:
            self.ui.label_file_opened.setText('Please select a data file')
        else:
            self.input_dir = inputfile
            self.output_dir = inputfile
            # fetch parameters from the GUI
            p_rfo = self.guiio.raw_file_opt_getter()
            p_es = self.guiio.exp_settings_getter()
            p_ls = self.guiio.load_save_getter()

            self.sample_rate = p_es['sample_rate']

            # self.UI_config_raw()
            # self.UI_config_light()
            # TODO: why the process events ? Just to display the loading data ??
            self.ui.label_file_opened.setText('<font color = Red>Loading data</font>')
            QCoreApplication.processEvents()

            # TODO fromfile is not a preferred option for reading data: it's platform dependent
            #  tdms and hdf5 would be better and compatible with both labview and python
            # load data from inputfile: either in binary format, or in raw text
            if p_ls['binaryfile']:
                tmp = np.fromfile(inputfile, dtype=np.float32, count=-1, sep="")
            else:
                tmp = np.fromfile(inputfile, dtype=float, count=-1, sep=" ")
            size = tmp.shape[0]
            # reshape the array in place without copy, from the given # of columns
            tmp.shape = (size // p_rfo['colnb'], p_rfo['colnb'])
            # Initialize the reformatted data array, with the same type of float as tmp (e.g. 32 or 64 bits)
            # contains: time, dbdt, ref data from N channels... , data from N channels...,
            # TODO transpose all the 'data' instead of repeated columns accesses!
            del self.data
            nb_of_datcol = p_rfo['coldatai'].shape[0]
            self.data = np.zeros(shape=(tmp.shape[0], p_rfo['colrefi'].shape[0] + nb_of_datcol + 3),
                                 dtype=tmp.dtype)
            # 1st column is time
            self.data[:, 0] = np.arange(tmp.shape[0])/p_es['sample_rate']
            # 2nd column is dbdt, centered on mean value
            self.data[:, 1] = tmp[:, p_rfo['coldbdt'] - 1] - tmp[:, p_rfo['coldbdt'] - 1].mean()

            # the next 'self.colref.shape[0]' columns are reference data, taking several lockins into account
            self.colref = p_rfo['colrefi']
            nb_of_refcol = self.colref.shape[0]
            self.freq = np.zeros(nb_of_refcol)
            for i in range(nb_of_refcol):
                self.data[:, i+2] = tmp[:, self.colref[i]-1] - np.mean(tmp[:, self.colref[i]-1])
                self.colref[i] = i+2
                # On calcule les fréquences de modulation de chaque lock-in
                # On approche la valeur avec une FFT
                data_fourier = np.abs(np.real(sp.fftpack.fft(self.data[:, i+2])))
                freq = sp.fftpack.fftfreq(len(self.data[:, i+2]), 1/p_es['sample_rate'])
                posj = np.argmax(data_fourier[0:round(len(data_fourier)/2)+1])
                self.freq[i] = freq[posj]
                # On effectue une DFT pour affiner le calcul
                deltaf = (freq[2]-freq[1])/1000
                fourier_length = 16084

                F = [0]*3
                expon = -2j*np.pi*self.data[0:fourier_length, 0]

                F[1] = abs(sp.integrate.trapz(self.data[0:fourier_length,
                                              self.colref[i]]*np.exp(expon*self.freq[i]),
                                              self.data[0:fourier_length, 0]))
                F[2] = abs(sp.integrate.trapz(self.data[0:fourier_length,
                                              self.colref[i]]*np.exp(expon*(self.freq[i]+deltaf)),
                                              self.data[0:fourier_length, 0]))
                F[0] = abs(sp.integrate.trapz(self.data[0:fourier_length,
                                              self.colref[i]]*np.exp(expon*(self.freq[i]-deltaf)),
                                              self.data[0:fourier_length, 0]))

                if F[2]>F[1]:
                    essaimax = F[1]
                    while abs(deltaf)>0.0002:
                        F[2] = abs(sp.integrate.trapz(self.data[0:fourier_length,
                                                      self.colref[i]]*np.exp(expon*(self.freq[i]+deltaf)),
                                                      self.data[0:fourier_length, 0]))
                        if F[2]>essaimax:
                            essaimax = F[2]
                            self.freq[i] = self.freq[i]+deltaf
                        else:
                            deltaf = -deltaf/10
                elif F[0]>F[1]:
                    deltaf = -deltaf
                    essaimax = F[1]
                    while abs(deltaf)>0.0002:
                        F[0] = abs(sp.integrate.trapz(self.data[0:fourier_length, self.colref[i]]*np.exp(expon*(self.freq[i]+deltaf)), self.data[0:fourier_length, 0]))
                        if F[0]>essaimax:
                            essaimax = F[0]
                            self.freq[i] = self.freq[i]+deltaf
                        else:
                            deltaf = -deltaf/10

            # the next 'self.colref.shape[0]' columns contain data
            self.coldata = np.zeros(nb_of_datcol, dtype=int)
            self.phasemin = np.zeros(nb_of_datcol)
            self.phasemini = np.zeros(nb_of_datcol)
            for i in range(nb_of_datcol):
                self.data[:, i + 2 + nb_of_refcol] = tmp[:, self.coldatai[i] - 1]
                self.coldata[i] = i + 2 + nb_of_refcol

            # Enfin la dernière colonne contient le champ intégré
            # calcul offset pick-up sur les 5% derniers points
            moyenne = np.mean(self.data[int(0.95*len(self.data)):,1])
            self.data[:, 1] = self.data[:, 1] - moyenne     # on soustrait l'offest
            # integration
            self.data[1:, 2 + len(self.coldata) + nb_of_refcol] = sp.integrate.cumtrapz(self.data[:, 1],
                                                                                        self.data[:, 0])
            # calcul champ à t_fin
            moyenne = np.mean(self.data[int(0.95*len(self.data)):, 2 + len(self.coldata) + nb_of_refcol])
            # on soustrait le champ final pour que Bfinal=0
            self.data[1:, 2 + len(self.coldata) + nb_of_refcol] = self.data[1:, 2 + len(self.coldata) + nb_of_refcol]\
                                                                  - moyenne
            #on calcule la valeur absolue pour avoir un champ postif et on stocke dans tempo
            self.tempo = abs(self.data[1:, 2 + len(self.coldata) + nb_of_refcol])
            self.f_max = abs(self.tempo).argmax()
            # argmax :Returns the indices of the maximum values along an axis.
            B_max = self.tempo[self.f_max] / self.pu_area
            print('B max : ',  round(B_max, 2))
            # print("10: affichage du bruit\n")
            # print("11: autoscale signal \n")
            # print("12: autoscale signal brut\n")

            B_max = abs(B_max)

            self.data[1:,
                      2 + len(self.coldata) + nb_of_refcol] = abs(self.data[1:, 2 + len(self.coldata) + nb_of_refcol])


            if B_max<200: # DV : je ne comprends pas à quoi sert le else ????
                self.f_start = 1
                self.f_stop = len(self.data[:, 0])
            else:
                for i in range(0, int(np.floor(len(self.data[:, 1])/100))):
                    if abs(np.mean(self.data[i*100:i*100+99, 2 + len(self.coldata) + nb_of_refcol]))>1e-3*self.data[self.f_max, 2 + len(self.coldata) + nb_of_refcol]:
                        self.f_start = (i-1)*100
                        break
                for i in range(int(self.f_max/100+10), int(np.floor(len(self.data[:, 1])/100))):
                    if abs(np.mean(self.data[i*100:i*100+99, 2 + len(self.coldata) + nb_of_refcol]))<1e-3*self.data[self.f_max, 2 + len(self.coldata) + nb_of_refcol]:
                        self.f_stop = (i+3)*100
                        break

            # On affiche par défaut les infos relatives au premier échantillon
            self.ui.ed_modrate.setText(str(float(int(self.freq[0]*100))/100))
            QCoreApplication.processEvents()
            # time.sleep(0.01)
            self.ui.cmb_sample.clear()
            for i in range(0, len(self.coldata)):
                self.ui.cmb_sample.addItem(str(i+1))
            self.spiked = 0
            self.ui.cb_dbdtsub.setChecked(False)
            self.dbdted = 0
            self.data_loaded = 1
            self.data_analyze()
            self.plot_analyzed()
            self.plot_raw()
            if B_max<0.5:
                self.ui.label_file_opened.setText('Low field detected, check dBdt column or PU area.')
            else:
                self.ui.label_file_opened.setText(str(inputfile))

    def f_save_data(self, outputfile):
        """ Save analysis data to ASCII """
        if outputfile != '':
            self.output_dir = outputfile
            if self.input_dir == '':
                self.input_dir = self.output_dir

            thinout = int(self.ui.ed_thin_out.text())

            # Sauvegarde de la montee du champ uniquement
            if self.ui.cb_up.isChecked() and not(self.ui.cb_down.isChecked()):
                out_data = np.zeros((np.floor(len(self.data[0:self.f_max:thinout, 0])), 3+2*len(self.coldata)))
                out_data[:, 0] = self.data[0:self.f_max:thinout, 0]
                out_data[:, 1] = self.data[0:self.f_max:thinout, 2+self.colref.shape[0]+len(self.coldata)]/self.pu_area
                out_data[:, 2] = self.data[0:self.f_max:thinout, 1]
                for j in range(len(self.coldata)):
                    out_data[:, 2*j+3] = self.sig_out[0:self.f_max:thinout, 2*j]/self.intgain*1e3
                    out_data[:, 2*j+4] = self.sig_out[0:self.f_max:thinout, 2*j+1]/self.intgain*1e3

            # Sauvegarde de la descente du champ uniquement
            elif self.ui.cb_down.isChecked() and not(self.ui.cb_up.isChecked()):
                out_data = np.zeros((np.floor(len(self.data[self.f_max::thinout, 0])), 3+2*len(self.coldata)))
                out_data[:, 0] = self.data[self.f_max::thinout, 0]
                out_data[:, 1] = self.data[self.f_max::thinout, 2+self.colref.shape[0]+len(self.coldata)]/self.pu_area
                out_data[:, 2] = self.data[self.f_max::thinout, 1]
                for j in range(0, len(self.coldata)):
                    out_data[:, 2*j+3] = self.sig_out[self.f_max::thinout, 2*j]/self.intgain*1e3
                    out_data[:, 2*j+4] = self.sig_out[self.f_max::thinout, 2*j+1]/self.intgain*1e3
            # Sinon on sauvegarde tout
            # Sauvegarde de la montee et de la descente du champ
            else:
                out_data = np.zeros((len(self.data[0::thinout]), 3+2*len(self.coldata)))
                out_data[:, 0] = self.data[0::thinout, 0]
                out_data[:, 1] = self.data[0::thinout, 2 + self.colref.shape[0] + len(self.coldata)] / self.pu_area
                out_data[:, 2] = self.data[0::thinout, 1]
                for j in range(len(self.coldata)):
                    out_data[:, 2*j+3] = self.sig_out[0::thinout, 2*j] / self.intgain * 1e3
                    out_data[:, 2*j+4] = self.sig_out[0::thinout, 2*j+1] / self.intgain * 1e3

            with open(str(outputfile), 'w') as f_handle:
                title = ['# time', 'B', 'dBdt','in_phase', 'out_phase']
                f_handle.write('\t'.join(title)+'\n')
                # TODO: why the slice on rows from the size of a column ??? No it was just useless '[:, 0]'
                # TODO: still isnt clear why skip the last two columns
                np.savetxt(f_handle, out_data[:len(out_data)-2], fmt = '%10g', delimiter = '\t')

    def f_savefile(self):
        if self.output_dir == '':
            outputfile, _ = QFileDialog.getSaveFileName(self, "Save Data File")
        else:
            outputfile, _ = QFileDialog.getSaveFileName(self, "Save Data File", self.output_dir)
        self.f_save_data(outputfile)
        outputfile_mod = outputfile.split('.')
        self.f_saveconfig(outputfile_mod[0] + ".conf")

    def f_batch(self):
        ''' Batch processing '''
        inputfile, _ = QFileDialog.getOpenFileNames(self, "Open Raw Data Files")
        print(inputfile)
        #Si des fichiers ont été choisi on lace le traitement
        if inputfile!= '':
            self.UI_config_raw()
            self.data_loaded = 1

            self.ui.label_status.setText('<font color = Red>Batch Treatment In Progress</font>')
            QCoreApplication.processEvents()
            time.sleep(0.01)

            # Chargement de chacun des fichiers et sauvegarde
            for input_i in range(0, len(inputfile)):
                self.f_load_data(str(inputfile[input_i]))
                self.f_save_data(str(inputfile[input_i])+'_out.dat')
                self.f_saveconfig(str(inputfile[input_i])+'_out.conf')

                self.label_22.setText(str(input_i+1)+'/'+str(len(inputfile)))
                QCoreApplication.processEvents()
                time.sleep(.01)
        else:
            self.ui.label_file_opened.setText('Please select a data file')

        self.ui.label_status.setText('<font color = Green>Batch Treatment Done</font>')

    def UI_cutoffHz(self):
        ''' Adjust time constant based on cuttof frequency '''
        self.cutoff = float(self.ui.ed_cutoff.text())
        if self.cutoff == 0:
            self.ui.ed_TC.setText('0')
        else:
            self.ui.ed_TC.setText(str(int(1e9/(2*np.pi*self.cutoff))/1000))

        if self.data_loaded == 1:
            self.data_analyzefast()
            self.plot_analyzed()

    def UI_cutoffms(self):
        ''' Adjust cutoff frequency based on time constant '''
        self.time_cste = float(self.ui.ed_TC.text())
        global k #DV
        if self.time_cste ==333:
            k=0
        if self.time_cste == 0:
            self.ui.ed_cutoff.setText('0')
            self.cutoff = 0

        else:
            self.ui.ed_cutoff.setText(str(int(1e9/(2*np.pi*self.time_cste))/1000))
            self.cutoff = float(self.ui.ed_cutoff.text())

        if self.data_loaded == 1:
            self.data_analyzefast()
            self.plot_analyzed()

    def UI_config_light(self):
        ''' Get constant from field values '''
        self.sample_rate = float(self.ui.ed_srate.text())
        self.dephase = float(self.ui.ed_manphase.text())
        self.pu_area = float(self.ui.ed_PUarea.text())
        self.gain = float(self.ui.ed_gain.text())
        self.current = float(self.ui.ed_current.text())

    def UI_config(self):
        ''' Get constant from field values and analyze data accordingly '''
        self.sample_rate = float(self.ui.ed_srate.text())
        self.dephase = float(self.ui.ed_manphase.text())
        self.pu_area = float(self.ui.ed_PUarea.text())
        self.gain = float(self.ui.ed_gain.text())
        self.current = float(self.ui.ed_current.text())
        if self.data_loaded == 1:
            self.data_analyzefast()
            self.plot_analyzed()
            self.plot_raw()

    def UI_config_raw(self):
        ''' Get raw file constant from GUI '''
        self.colnb = int(self.ui.ed_ncol.text())
        self.coldbdt = int(self.ui.ed_dBdtcol.text())

        tempstring = self.ui.ed_datcol.text()
        tempstring = tempstring.split(', ')
        self.coldatai = np.zeros(len(tempstring),dtype=int)
        for i in range(0, len(tempstring)):
            self.coldatai[i] = int(tempstring[i])

        del tempstring
        tempstring = self.ui.ed_refcol.text()
        tempstring = tempstring.split(', ')
        self.colrefi = np.zeros(len(tempstring),dtype=int)
        for i in range(0, len(tempstring)):
            self.colrefi[i] = tempstring[i]

    def plot_lpass(self):
        ''' If data already loaded, execute analysis '''
        if self.data_loaded == 1:
            self.data_analyze()
            self.plot_analyzed()

    def plot_combobox(self):
        ''' If data loaded, refresh plots '''
        if self.data_loaded == 1:
            self.plot_analyzed()
            self.plot_raw()

    def plot_analyzed(self):
        ''' Plot analyzed data '''
        # On affiche la bonne fréquence de modulation
        act_sample = int(self.ui.cmb_sample.currentIndex())
        if self.colref.shape[0] == 1:
            thefreq = self.freq[0]
        else:
            thefreq = self.freq[act_sample]

        self.ui.ed_modrate.setText(str(float(int(thefreq*100))/100))
        QCoreApplication.processEvents()
        # time.sleep(0.01)

        # On choisi les abscisses en fonction du choix utilisateur
        if self.ui.rb_RB.isChecked():
            if self.ui.cb_up.isChecked() and self.ui.cb_down.isChecked():
                x_space = abs(self.data[self.f_start:self.f_stop:10, 2+self.colref.shape[0]+len(self.coldata)]/self.pu_area)
            elif self.ui.cb_up.isChecked():
                x_space = abs(self.data[self.f_start:self.f_max:10, 2+self.colref.shape[0]+len(self.coldata)]/self.pu_area)
            else:
                x_space = abs(self.data[self.f_max:self.f_stop:10, 2+self.colref.shape[0]+len(self.coldata)]/self.pu_area)

            # self.qwtPlot_2.setAxisTitle(Qwt.QwtPlot.xBottom, "Field (T)")
        else:
            x_space = self.data[self.f_start:self.f_stop:10, 0]
            # self.qwtPlot_2.setAxisTitle(Qwt.QwtPlot.xBottom, "time (s)")

        # On sélectionne les données en fonction de l'échantillon
        # On prend en compte le gain et le courant d'excitation
        if self.ui.cb_rms.isChecked():
            self.intgain = 0.5*self.amplitude[act_sample]*(self.gain*self.current*np.sqrt(2))
        else:
            self.intgain = 0.5*self.amplitude[act_sample]*(self.gain*self.current)

        if self.ui.cb_up.isChecked() and self.ui.cb_down.isChecked():
            y_space = self.sig_out[self.f_start:self.f_stop:10, 2*(act_sample)]/self.intgain*1e3
            y_space_2 = self.sig_out[self.f_start:self.f_stop:10, 2*(act_sample)+1]/self.intgain*1e3
        elif self.ui.cb_up.isChecked():
            y_space = self.sig_out[self.f_start:self.f_max:10, 2*(act_sample)]/self.intgain*1e3
            y_space_2 = self.sig_out[self.f_start:self.f_max:10, 2*(act_sample)+1]/self.intgain*1e3
        else:
            y_space = self.sig_out[self.f_max:self.f_stop:10, 2*(act_sample)]/self.intgain*1e3
            y_space_2 = self.sig_out[self.f_max:self.f_stop:10, 2*(act_sample)+1]/self.intgain*1e3

        # Erase previous plots
        self.ui.plot_signal_left.clear()
        self.ui.plot_signal_right.clear()
        # TODO: no clear all in pyqtgraph legend, to implement oneself, also the online doc is not up to date:
        #  the items are now found by 'name' (see source extract below)
        self.ui.plot_signal_left.getPlotItem().legend.removeItem("Raw Voltage")
        self.ui.plot_signal_right.getPlotItem().legend.removeItem("In phase")
        self.ui.plot_signal_right.getPlotItem().legend.removeItem("Out of phase")
        # for item in self.ui.plot_signal_left.getPlotItem().legend.items:
        # for sample, label in self.items:
        #     if label.text == name:  # hit
        #         self.items.remove( (sample, label) )    # remove from itemlist
        #         self.layout.removeItem(sample)          # remove from layout
        #         sample.close()                          # remove from drawing
        #         self.layout.removeItem(label)
        #         label.close()
        #         self.updateSize()                       # redraq box
        #
        #         self.ui.plot_signal_left.getPlotItem().legend.items = []
        #         self.ui.plot_signal_left.getPlotItem().legend.updateSize()
        # # print(self.ui.plot_signal_left.getPlotItem().legend.items)
        # # for item in self.ui.plot_signal_left.getPlotItem().legend.items:
        # #     print(item, type(item))
        # #     self.ui.plot_signal_left.getPlotItem().legend.removeItem(item)
        # # print(self.ui.plot_signal_left.getPlotItem().legend.items)



        # On dimensionne les axes à la main

        # y_space_bound = [min(y_space), min(y_space_2), max(y_space), max(y_space_2)]

        # if self.ui.cb_inphase.isChecked() and self.ui.cb_outphase.isChecked() :
            # if k==0:
            #     self.ui.plot_signal_right.setYRange(min(y_space_bound), max(y_space_bound))
                # self.qwtPlot_2.setAxisScale(Qwt.QwtPlot.yLeft, min(y_space_bound), max(y_space_bound))
                # k=1
        # elif self.ui.cb_outphase.isChecked():
        #     self.ui.plot_signal_right.setYRange(y_space_bound[1], y_space_bound[3])
            # self.qwtPlot_2.setAxisScale(Qwt.QwtPlot.yLeft, y_space_bound[1], y_space_bound[3])
        # elif self.ui.cb_inphase.isChecked():
        #     self.ui.plot_signal_right.setYRange(y_space_bound[0], y_space_bound[2])
            # self.qwtPlot_2.setAxisScale(Qwt.QwtPlot.yLeft, y_space_bound[0], y_space_bound[2])

        # On réaffiche le nouveau
        if self.ui.cb_inphase.isChecked():
            self.curve = self.ui.plot_signal_right.plot(pen='y',name="In phase")
            self.curve.setData(x_space, y_space)
        if self.ui.cb_outphase.isChecked():
            self.curve = self.ui.plot_signal_right.plot(pen='r', name="Out of phase")
            self.curve.setData(x_space, y_space_2)

    def plot_raw(self):
        ''' Plot raw data '''
        self.ui.plot_field_left.clear()
        self.ui.plot_field_right.clear()
        self.ui.plot_signal_left.getPlotItem().legend.removeItem("Raw Voltage")
        self.ui.plot_field_left.getPlotItem().legend.removeItem("Ref. Voltage")
        self.ui.plot_field_right.getPlotItem().legend.removeItem("Field")
        self.ui.plot_field_right.getPlotItem().legend.removeItem("Pick-up")
        self.ui.plot_field_left.setDownsampling(auto=True, mode='peak')
        self.ui.plot_field_right.setDownsampling(auto=True, mode='peak')
        self.ui.plot_field_left.setClipToView(True)
        self.ui.plot_field_right.setClipToView(True)

        longueur = len(self.data[:, 0])

        act_sample = int(self.ui.cmb_sample.currentIndex())

        # y_space = self.data[1:longueur:4, 2+self.colref.shape[0]+act_sample]
        y_space = self.data[:, 2 + self.colref.shape[0] + act_sample]

        if self.colref.shape[0] == 1:
            # on prend que 1 point sur 9 pour ne pas surcharger l'affichage
            y_space_2 = self.data[:, self.colref[0]]
        else:
            # 1: longueur:9
            y_space_2 = self.data[:, self.colref[act_sample]]


        # Champ integre et pickup
        # 1: longueur:10
        y_space_3 = self.data[:, 2+len(self.coldata)+self.colref.shape[0]]/self.pu_area
        y_space_4 = max(y_space_3)*self.data[:, 1]/max(self.data[:, 1])

        self.curve = self.ui.plot_signal_left.plot(pen='y', name="Raw Voltage")
        # 1: longueur:4
        self.curve.setData(self.data[:, 0], y_space)

        self.curve = self.ui.plot_field_left.plot(pen='y', name="Ref. Voltage")
        self.curve.setData(self.data[:, 0], y_space_2)

        #Initialisation de la base de zoom (zoom out max)
        self.curve = self.ui.plot_field_right.plot(pen='y', name="Field")
        # 0::10
        self.curve.setData(self.data[:, 0], y_space_3)
        self.curve_2 = self.ui.plot_field_right.plot(pen='b', name="Pick-up")
        self.curve_2.setData(self.data[:, 0], y_space_4)
        print('graph updated')

    def data_analyze(self):
        """ Treat raw data """
        self.sig_out = np.zeros((len(self.data[:, 2]), len(self.coldata)*2))
        phase_ref = np.zeros(len(self.data[:, 2]))
        antiphase_ref = np.zeros(len(self.data[:, 2]))
        self.amplitude = np.zeros(len(self.coldata))
        self.zp = int(self.ui.ed_phase_accuracy.text())

        for j in range(len(self.coldata)):
            if self.colref.shape[0] == 1:
                posref = self.colref
                thefreq = self.freq[0]
            else:
                posref = self.colref[j]
                thefreq = self.freq[j]

            # Phase detector
            self.amplitude[j] = (max(self.data[1:10000, int(posref)])-min(self.data[1:10000, int(posref)]))/2
            phase_ref[:] = self.amplitude[j]*np.cos(2*np.pi*thefreq*self.data[:, 0])

            # Phase detector using sinusoidal fit
            fitfunc       = lambda p, x: self.amplitude[j]*np.cos(2*np.pi*thefreq*x+p[0])
            errfunc       = lambda p, x, y: fitfunc(p, x) - y
            p0            = [0]
            fit_le        = int(self.zp*round(self.sample_rate/thefreq))
            # full output=1: p1, cov_x, infodict, msg, success
            # full output=0: p1, success
            p1, _, _, _, success = sp.optimize.leastsq(errfunc,
                                    np.array(p0[:]),
                                    args=(self.data[0:fit_le,0],
                                          self.data[0:fit_le, int(posref)]),
                                    full_output=1
                                    )
            # print(p1)
            self.phasemin = p1*180/np.pi
            phase_ref[:] = self.amplitude[j]*np.cos(2*np.pi*thefreq*self.data[:, 0]+self.phasemin[j]*np.pi/180+self.dephase*np.pi/180)
            # print(self.sig_out[:, 2*j].shape)
            # print(phase_ref)
            # print(phase_ref.shape)
            # print(phase_ref[:])
            # print(self.coldata[j])
            # print(self.data[:, self.coldata[j]])
            # print(phase_ref[:]*self.data[:, self.coldata[j]])
            self.sig_out[:, 2*j] = phase_ref[:] * self.data[:, self.coldata[j]]
            self.sig_out[:, 2*j] = self.data_lpass(self.sig_out[:, 2*j], self.cutoff, self.sample_rate)

            antiphase_ref[:] = -self.amplitude[j]*np.sin(2*np.pi*thefreq*self.data[:, 0]+self.phasemin[j]*np.pi/180+self.dephase*np.pi/180)
            self.sig_out[:, 2*j+1] = antiphase_ref[:]*self.data[:, self.coldata[j]]
            self.sig_out[:, 2*j+1] = self.data_lpass(self.sig_out[:, 2*j+1], self.cutoff, self.sample_rate)

    def data_analyzefast(self):
        ''' Same as data_analyze without phase detection '''
        phase_ref = np.zeros(len(self.data[:, 2]))
        antiphase_ref = np.zeros(len(self.data[:, 2]))
        self.sig_out = np.zeros((len(self.data[:, 2]), len(self.coldata)*2))

        for j in range(len(self.coldata)):
            if self.colref.shape[0] == 1:
                thefreq = self.freq[0]
                posref = self.colref
            else:
                thefreq = self.freq[j]
                posref = self.colref[j]

            self.amplitude[j] = (max(self.data[1:1000, int(posref)])-min(self.data[1:1000, int(posref)]))/2
            antiphase_ref[:] = -self.amplitude[j]*np.sin(2*np.pi*thefreq*self.data[:, 0]+self.phasemin[j]*np.pi/180+self.dephase*np.pi/180)
            phase_ref[:] = self.amplitude[j]*np.cos(2*np.pi*thefreq*self.data[:, 0]+self.phasemin[j]*np.pi/180+self.dephase*np.pi/180)
            self.sig_out[:, 2*j+1] = antiphase_ref[:]*self.data[:, self.coldata[j]]
            self.sig_out[:, 2*j+1] = self.data_lpass(self.sig_out[:, 2*j+1], self.cutoff, self.sample_rate)
            self.sig_out[:, 2*j] = phase_ref[:]*self.data[:, self.coldata[j]]
            self.sig_out[:, 2*j] = self.data_lpass(self.sig_out[:, 2*j], self.cutoff, self.sample_rate)

    def data_lpass(self, x, Wp, srate):
        ''' Low-pass filter using various filter type '''
        tempstring = self.ui.ed_filt_order.text()

        if tempstring == 'auto':
            Wp = float(Wp*2/srate)
            Ws = Wp*float(self.ui.ed_stopband_freq.text())
            Rp = float(self.ui.ed_passband_atten.text())
            Rs = float(self.ui.ed_stopband_atten.text())

            if self.ui.cmb_filttype.currentIndex() == 0:
                (norder, Wn) = signal.buttord(Wp, Ws, Rp, Rs)
            elif self.ui.cmb_filttype.currentIndex() == 1:
                (norder, Wn) = signal.ellipord(Wp, Ws, Rp, Rs)
            else:
                (norder, Wn) = signal.cheb1ord(Wp, Ws, Rp, Rs)

        else:
            norder = float(tempstring)
            Wp = float(Wp*2/srate)
            Ws = Wp*2
            self.ui.ed_stopband_freq.setText(str(Ws/Wp))
            Rp = 3
            self.ui.ed_passband_atten.setText(str(Rp))
            Rs = 0.3*norder*20
            self.ui.ed_stopband_atten.setText(str(Rs))

            if self.ui.cmb_filttype.currentIndex() == 0:
                (norder, Wn) = signal.buttord(Wp, Ws, Rp, Rs)
            elif self.ui.cmb_filttype.currentIndex() == 1:
                (norder, Wn) = signal.ellipord(Wp, Ws, Rp, Rs)
            else:
                (norder, Wn) = signal.cheb1ord(Wp, Ws, Rp, Rs)

        if self.ui.cmb_filttype.currentIndex() == 0:
            (b, a)  =  modified_filter.butter(norder, Wn)
        elif self.ui.cmb_filttype.currentIndex() == 1:
            (b, a)  =  signal.ellip(norder, Rp, Rs, Wn)
        else:
            (b, a)  =  signal.cheby1(norder, Rp, Wn)


        y  =  signal.filtfilt(b, a, x)
        return(y)

    def data_hpass(self, x, Wp, srate):
        ''' High-pass filter '''
        Wp = float(Wp*2/srate)
        Ws = Wp*float(self.ui.ed_stopband_freq.text())
        Rp = float(self.ui.ed_passband_atten.text())
        Rs = float(self.ui.ed_stopband_atten.text())

        tempstring = self.ui.ed_filt_order.text()
        if tempstring == 'auto':
            if self.ui.cmb_filttype.currentIndex() == 0:
                (norder, Wn) = signal.buttord(Wp, Ws, Rp, Rs)
            elif self.ui.cmb_filttype.currentIndex() == 1:
                (norder, Wn) = signal.ellipord(Wp, Ws, Rp, Rs)
            else:
                (norder, Wn) = signal.cheb1ord(Wp, Ws, Rp, Rs)
        else:
            norder = float(tempstring)
            Wn = Wp

        if self.ui.cmb_filttype.currentIndex() == 0:
            (b, a)  =  modified_filter.butter(norder, Wn, btype ='high')
        elif self.ui.cmb_filttype.currentIndex() == 1:
            (b, a)  =  signal.ellip(norder, Rp, Rs, Wn)
        else:
            (b, a)  =  signal.cheby1(norder, Rp, Wn)


        y  =  signal.filtfilt(b, a, x)

        return(y)

    def data_artifact(self):
        """ Suppress dB/dt from raw signal """
        if self.data_loaded == 1:
            if self.dbdted == 0 and self.spiked == 0:
                self.data_raw = np.zeros((len(self.data[:, 0]), len(self.data[0, :])))
                self.data_raw[:, :] = self.data[:, :]
            print("ok")


            if self.ui.cb_dbdtsub.isChecked():
                # dbdt removal : high-pass filter on raw data
                self.dbdted = 1
                tempstring = self.ui.ed_dbdt_hp_cutoff.text()
                if tempstring == 'auto':
                    dbdt_cutoff = min(self.freq)/20
                else:
                    dbdt_cutoff = float(self.ui.ed_dbdt_hp_cutoff.text())

                self.ui.label_status.setText('<font color = Red>Calculations In Progress</font>')
                QCoreApplication.processEvents()
                time.sleep(0.01)
                for i in range(0, len(self.coldata)):
                    self.data[:, self.coldata[i]] = self.data_hpass(self.data_raw[:, self.coldata[i]], dbdt_cutoff, self.sample_rate)

            if not self.ui.cb_dbdtsub.isChecked():
                self.data[:, :] = self.data_raw[:, :]
            self.data_analyze()
            self.plot_analyzed()
            self.plot_raw()

        self.ui.label_status.setText('')

    def check_update(self):
        pass

