# -*- coding: utf-8 -*-
import numpy as np

class Getset:
    def __init__(self, ui):
        self.ui = ui

    def raw_file_opt_setter(self, param):
        # Raw File Options Group Box
        self.ui.ed_ncol.setText(param['ed_ncol'])
        self.ui.ed_dBdtcol.setText(param['ed_dBdtcol'])
        self.ui.ed_datcol.setText(param['ed_datcol'])
        self.ui.ed_refcol.setText(param['ed_refcol'])

    def raw_file_opt_getter(self):
        # Raw File Options Group Box
        colnb = self.ui.ed_ncol.text()
        coldbdt = self.ui.ed_dBdtcol.text()
        coldatai = self.ui.ed_datcol.text()
        colrefi = self.ui.ed_refcol.text()
        param = {'ed_ncol': colnb, 'colnb': int(colnb), 'ed_dBdtcol': coldbdt, 'coldbdt': int(coldbdt),
                 'ed_datcol': coldatai, 'coldatai': np.fromstring(coldatai, dtype=int, sep=','), 'ed_refcol': colrefi,
                 'colrefi': np.fromstring(colrefi, dtype=int, sep=',')}
        return param

    def exp_settings_setter(self, param):
        # Experimental Settings Groupbox
        self.ui.ed_srate.setText(param['ed_srate'])
        self.ui.ed_PUarea.setText(param['ed_thin_out'])
        self.ui.ed_current.setText(param['ed_current'])
        self.ui.ed_gain.setText(param['ed_gain'])
        self.ui.cb_rms.setChecked(param['cb_rms'])

    def exp_settings_getter(self):
        # Load Save Groupbox
        srate = self.ui.ed_srate.text()
        pu_area = self.ui.ed_PUarea.text()
        current = self.ui.ed_current.text()
        gain = self.ui.ed_gain.text()
        rms = self.ui.cb_rms.isChecked()
        param = {'ed_srate': srate, 'sample_rate': float(srate), 'ed_PUarea': pu_area, 'pu_area': float(pu_area),
                 'ed_current': current, 'current': float(current), 'ed_gain': gain, 'gain': float(gain), 'cb_rms': rms}
        return param

    def ana_param_setter(self, param):
        # Analysis Parameters GroupBox
        self.ui.ed_cutoff.setText(param['ed_cutoff'])
        self.ui.ed_modrate.setText(param['ed_modrate'])
        self.ui.ed_TC.setText(param['ed_TC'])
        self.ui.ed_manphase.setText(param['ed_manphase'])
        self.ui.cb_dbdtsub.setChecked(param['cb_dbdtsub'])

    def ana_param_getter(self):
        # Analysis Parameters GroupBox
        cutoff = self.ui.ed_cutoff.text()
        modrate = self.ui.ed_modrate.text()
        tc = self.ui.ed_TC.text()
        manphase = self.ui.ed_manphase.text()
        checked = self.ui.cb_dbdtsub.isChecked()
        param = dict(ed_cutoff=cutoff, cutoff=float(cutoff), ed_modrate=modrate, ed_TC=tc, ed_manphase=manphase, dephase=float(manphase),
                     cb_dbdtsub=checked)
        return param

    def plot_opt_setter(self, param):
        # Plot Options Groupbox
        self.ui.rb_RB.setChecked(param['rb_RB'])
        self.ui.rb_Rt.setChecked(param['rb_Rt'])
        self.ui.cb_inphase.setChecked(param['cb_inphase'])
        self.ui.cb_outphase.setChecked(param['cb_outphase'])
        self.ui.cb_up.setChecked(param['cb_up'])
        self.ui.cb_down.setChecked(param['cb_down'])
        # cmb_sample

    def plot_opt_getter(self):
        # Plot Options Groupbox
        param = {'rb_RB': self.ui.rb_RB.isChecked(), 'rb_Rt': self.ui.rb_Rt.isChecked(),
                 'cb_inphase': self.ui.cb_inphase.isChecked(), 'cb_outphase': self.ui.cb_outphase.isChecked(),
                 'cb_up': self.ui.cb_up.isChecked(), 'cb_down': self.ui.cb_down.isChecked(),
                 'cmb_sample': self.ui.cmb_sample.currentText()}
        return param

    def load_save_setter(self, param):
        # Load Save Groupbox
        self.ui.ed_thin_out.setText(param['ed_thin_out'])
        self.ui.cb_binfile.setChecked(param['cb_binfile'])

    def load_save_getter(self):
        # Load Save Groupbox
        param = {'ed_thin_out': self.ui.ed_thin_out.text(), 'binaryfile': self.ui.cb_binfile.isChecked()}
        return param

    def adv_opt_tab_setter(self, param):
        """set the Advanced Options tab parameters from a dictionary"""
        self.ui.cmb_filttype.setCurrentText(param['cmb_filttype'])
        self.ui.ed_filt_order.setText(param['ed_filt_order'])
        self.ui.cmb_sigtype.setCurrentText(param['cmb_sigtype'])
        self.ui.ed_phase_accuracy.setText(param['ed_phase_accuracy'])
        self.ui.ed_passband_atten.setText(param['ed_passband_atten'])
        self.ui.ed_stopband_atten.setText(param['ed_stopband_atten'])
        self.ui.ed_stopband_freq.setText(param['ed_stopband_freq'])
        self.ui.ed_dbdt_hp_cutoff.setText(param['ed_dbdt_hp_cutoff'])

    def adv_opt_tab_getter(self):
        """Return a dictionary of the Advanced Options tab parameters"""
        param = {'cmb_filttype': self.ui.cmb_filttype.currentText(), 'ed_filt_order': self.ui.ed_filt_order.text(),
                 'cmb_sigtype': self.ui.cmb_sigtype.currentText(),
                 'ed_phase_accuracy': self.ui.ed_phase_accuracy.text(),
                 'ed_passband_atten': self.ui.ed_passband_atten.text(),
                 'ed_stopband_atten': self.ui.ed_stopband_atten.text(),
                 'ed_stopband_freq': self.ui.ed_stopband_freq.text(),
                 'ed_dbdt_hp_cutoff': self.ui.ed_dbdt_hp_cutoff.text()}
        return param
